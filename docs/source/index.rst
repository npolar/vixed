.. npolar-doc documentation master file, created by
   sphinx-quickstart on Fri Feb  2 21:55:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Vixed tutorial!
==========================

.. toctree::
   :glob:
   :maxdepth: 3
   :caption: Contents:

   intro
   processors
   requests
   data
   issues




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
