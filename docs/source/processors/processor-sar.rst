sar
---

Request with polygon ROI
========================

Sample request file contents where area of interest is described as custom polygon:

::

    {
        "interval": "1d",
        "processor": "SAR",
        "processor_settings": {
            "compress": "True",
            "crs": "EPSG:3035",
            "platformname": "Sentinel-1",
            "producttype": "GRD",
            "roi": {
                "coordinates": [
                    [
                        [
                            -9.954874050103166,
                            84.90358049250898
                        ],
                        [
                            3.350877020730215,
                            80.49145938744702
                        ],
                        [
                            38.483172690086676,
                            79.02782330052757
                        ],
                        [
                            59.008819416329565,
                            83.76614133556653
                        ],
                        [
                            -9.954874050103166,
                            84.90358049250898
                        ]
                    ]
                ],
                "type": "Polygon"
            },
            "sensoroperationalmode": "EW",
            "spatial_resolution": 550,
            "time_delta_hours": 24
        },
        "send_to": [
            "user@example.com"
        ],
        "start": [
            "1900-05-21T20:00:00Z"
        ],
        "stop": "1900-05-23T19:01:00Z"
    }

Request with center coordinates and box size
=============================================

Sample request file where area of interest is centered over a location and the size of the box is given in meters:

::

    {
        "interval": "1d",
        "processor": "SAR",
        "processor_settings": {
            "box_radius": 100000,
            "center_lat": 82.0,
            "center_lon": 25.5,
            "compress": "True",
            "crs": "EPSG:3035",
            "platformname": "Sentinel-1",
            "producttype": "GRD",
            "roi": "bbox",
            "sensoroperationalmode": "EW",
            "spatial_resolution": 50,
            "time_delta_hours": 20
        },
        "send_to": [
            "user@example.com"
        ],
        "start": [
            "2018-05-21T20:00:00Z"
        ],
        "stop": "2018-05-26T05:38:00Z"
    }

