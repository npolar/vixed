sentinel-2
----------

Processor `sentinel-2` for obtaining Sentinel-2 imagery. Currently it will only deliver single band data (band number is hardcoded and set to 2).

::

	{
		"start": [
			"1900-05-20T08:00:00Z"
		],
		"stop": "1900-05-26T02:30:00Z",
		"interval": "1d",
		"send_to": [
			"user@example.com"
		],
		"processor": "sentinel-2",
		"processor_settings": {
			"compress": "True",
			"crs": "EPSG:3035",
			"platformname": "Sentinel-2",
			"producttype": "",
			"roi": "bbox",
			"center_lon": 28.5,
			"center_lat": 82.5,
			"box_radius": 50000,
			"cloudcoverpercentage": 80,
			"spatial_resolution": 30,
			"time_delta_hours": 64
		}
	}
