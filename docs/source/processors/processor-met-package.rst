met-package
-----------

`met-package` processor will download and package three operational products:

* MET Norway TOPAZ ocean model forecast fields
* MET Norway ice charts
* MET Norway Arome weather model forecast fields

Currently the regional borders of the forecast files are set to the Northern Barents sea.

Sample request file:

::

	{
		"start": [
			"1900-05-20T00:00:00Z"
		],
		"stop": "1900-05-26T06:34:00Z",
		"interval": "1d",
		"send_to": [
			"user@example.com"
		],
		"processor": "met-package",
		"processor_settings": {}
	}

